﻿# Scandiweb
This is a completed Junior  Test for Scandiweb
----
## Requirements
>
PHP Version >=5.0 
>
WAMPServer or another analogs with MySQL and PHP support
----
##Getting Started
Input credentials of your servername, username, password adn dbname into **view.php**, **index.php**, **delete.php**:

>private $servername = "server";

>private $username   = "user";

>private $password   = "pass";

>private $dbname     = "example";

If you want to import some data, just open MySQL and import data from **products.sql**

----
## Changelog
* 10-05-2018 Added sublcasses into view.php. Renamed classes the same as the filenames. Added SASS functionality. Code is prettified. Added value deleting on product adding page to avoid bugs, when sending data to database.
