function displayForm() {
    var option = document.getElementById("select").value;
    if (option == "Book") {
        document.getElementById("size").required = false;
        document.getElementById("size").value = "";
        document.getElementById("weight").required = true;
        document.getElementById("height").required = false;
        document.getElementById("height").value = "";
        document.getElementById("width").required = false;
        document.getElementById("width").value = "";
        document.getElementById("length").required = false;
        document.getElementById("length").value = "";
        document.getElementById("book").style.display = "block";
        document.getElementById("disk").style.display = "none";
        document.getElementById("furniture").style.display = "none";
    }
    if (option == "DVD-disk") {
        document.getElementById("size").required = true;
        document.getElementById("weight").required = false;
        document.getElementById("weight").value = "";
        document.getElementById("height").required = false;
        document.getElementById("height").value = "";
        document.getElementById("width").required = false;
        document.getElementById("width").value = "";
        document.getElementById("length").required = false;
        document.getElementById("length").value = "";
        document.getElementById("book").style.display = "none";
        document.getElementById("disk").style.display = "block";
        document.getElementById("furniture").style.display = "none";
    }
    if (option == "Furniture") {
        document.getElementById("size").required = false;
        document.getElementById("size").value = "";
        document.getElementById("weight").required = false;
        document.getElementById("weight").value = "";
        document.getElementById("height").required = true;
        document.getElementById("width").required = true;
        document.getElementById("length").required = true
        document.getElementById("book").style.display = "none";
        document.getElementById("disk").style.display = "none";
        document.getElementById("furniture").style.display = "block";
    }

}

function deleteList() {
    document.getElementById("delete_list").submit();
}