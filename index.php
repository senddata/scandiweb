<html>

<head>
    <title>Product List</title>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="script.js"></script>
</head>

<body>
    <div>
        <div class="top_content">
            <table width=100%>
                <tr>
                    <th id="top_content_left">
                        <p>Product List</p>
                    </th>
                    <th id="top_content_right">
                        <select>
                            <option>Mass delete option</option>
                        </select>
                        <button id="apply" onclick="deleteList()">Apply</button>
                        <a href="add.html">Add Item</a>
                    </th>
                </tr>
            </table>
        </div>
        <hr>
        <div class="list_content">
            <table>
                <form action="delete.php" method=post id="delete_list">
                    <?php
$index = new index();
$index->ShowAll();
class index
{
    private $servername = "localhost";
    private $username   = "admin";
    private $password   = "Lol12345";
    private $dbname     = "products";
    private $conn       = "";

    public function __construct()
    {
        $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname); //make connection to mysql
    }

    public function ShowAll()
    {
        $counter = 0;
        $sql     = "SELECT * FROM products";
        $result  = $this->conn->query($sql);
        if ($result->num_rows > 0) {
            echo "<tr>"; //Show as a table
            while ($row = $result->fetch_assoc()) {
                if ($row["Type"] == "book") {
                    echo "<td><div><input type=\"checkbox\" name=" . $row["SKU"] . "><ul><li>" . $row["SKU"] . "</li><li>" . $row["Name"] . "</li><li>" . $row["Price"] . "€</li><li>" . $row["Weight"] . "KG</li><ul></div></td>";
                }
                if ($row["Type"] == "disk") {
                    echo "<td><div><input type=\"checkbox\" name=" . $row["SKU"] . "><ul><li>" . $row["SKU"] . "</li><li>" . $row["Name"] . "</li><li>" . $row["Price"] . "€</li><li>" . $row["Size"] . "MB</li><ul></div></td>";
                }
                if ($row["Type"] == "furniture") {
                    echo "<td><div><input type=\"checkbox\" name=" . $row["SKU"] . "><ul><li>" . $row["SKU"] . "</li><li>" . $row["Name"] . "</li><li>" . $row["Price"] . "€</li><li>Dimensions: " . $row["Height"] . "x" . $row["Width"] . "x" . $row["Length"] . "</li><ul></div></td>";
                }
                $counter++;
                if ($counter == 4) {
                    //if it is 4 containers in row, start new row
                    echo "</tr>";
                    $counter = 0;
                }
            }
        } else {
            echo "0 results";
        }

        if ($counter != 0) {
            echo "</tr>";
        }
        //need to end tag of <tr>
        $this->conn->close();
    }
}
?>
                </form>
            </table>
        </div>
</body>

</html>