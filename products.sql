-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 10, 2018 at 09:25 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `products`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `SKU` varchar(20) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `Price` decimal(10,2) NOT NULL,
  `Type` varchar(10) NOT NULL,
  `Weight` decimal(11,2) DEFAULT NULL,
  `Height` int(11) DEFAULT NULL,
  `Width` int(11) DEFAULT NULL,
  `Length` int(11) DEFAULT NULL,
  `Size` int(11) DEFAULT NULL,
  PRIMARY KEY (`SKU`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`SKU`, `Name`, `Price`, `Type`, `Weight`, `Height`, `Width`, `Length`, `Size`) VALUES
('PUP992', 'Assassins Creed', '99.00', 'disk', NULL, NULL, NULL, NULL, 12),
('PIPO1112', 'Pikotaro', '12.00', 'disk', NULL, NULL, NULL, NULL, 41),
('SOP192', 'Titanic', '199.00', 'furniture', NULL, 14, 128, 122, NULL),
('TR1991', 'Transformers', '110.00', 'disk', NULL, NULL, NULL, NULL, 79000),
('CUSA1992', 'God of War', '50.00', 'disk', NULL, NULL, NULL, NULL, 99000),
('CUSA1892', 'Ratchet and Clank', '49.99', 'disk', NULL, NULL, NULL, NULL, 99000);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
